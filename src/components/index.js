import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { title } from '../config'

export const tagify = name => name.toLowerCase().replace(' ', '-');
export const getShipmentByTag = (shipments, tag) => shipments.filter(shipment => tagify(shipment.name) === tag)[0];
export const calculateBays = boxes => Math.ceil(boxes ? boxes.split(',').filter(box => parseFloat(box)).reduce((a, b) => parseFloat(a) + parseFloat(b), 0) / 10 : 0);
export const byShipmentName = (a, b) => (a.name > b.name) ? 1 : ((a.name < b.name) ? -1 : 0);

export function Shipment (props) {
  const { tag } = useParams();
  const shipment = getShipmentByTag(props.shipments, tag);
  const index = props.shipments.indexOf(shipment)
  const [bays, setBays] = useState(calculateBays(shipment.boxes));

  useEffect(() => document.title = title + ' - ' + shipment.name, [shipment.name]);

  function onBoxesChanged (event) {
    const updated = {
      id: shipment.id,
      name: shipment.name,
      email: shipment.email,
      boxes: event.target.value
    };
    setBays(calculateBays(updated.boxes));
    props.onUpdate(updated, index);
  }
  
  return <article>
      <h1>{shipment.name}</h1>
      <p><a href={`mailto:${shipment.email}`}>{shipment.email}</a></p>
      <p>Number of required cargo bays <b>{bays}</b></p>
      <p>
        <label htmlFor="shipment-boxes">Cargo boxes</label> <input id ="shipment-boxes" value={shipment.boxes || ''} onChange={onBoxesChanged}/>
      </p>
    </article>;
}