import React, { useState, useEffect } from 'react';
import { Switch, Route, Link, useLocation } from "react-router-dom";
import { Shipment, tagify, byShipmentName } from './components';
import { title, content } from './config';
import './App.css';

function App () {
  const stored = localStorage.getItem('shipments');
  const { pathname } = useLocation();
  const [shipments, setShipments] = useState(stored ? JSON.parse(stored) : []);
  const [query, setQuery] = useState('');
  const [found, setFound] = useState(shipments);
  const ready = shipments => !document.documentElement.classList.remove('-loading') &&!localStorage.setItem('shipments', JSON.stringify(shipments)) && !setShipments(shipments);
  const load = event => !document.documentElement.classList.add('-loading') && fetch(content).then(response => response.json()).then(result => ready(result));
  const save = event => localStorage.setItem('shipments', JSON.stringify(shipments));
  const onShipmentUpdate = (shipment, index) => setShipments(shipments.map((s, i) => i === index ? shipment : s));
  const onSearchInput = event => setQuery(event.target.value);
  
  useEffect(() => document.title = title, []);
  useEffect(() => setFound(query ? shipments.filter(shipment => shipment.name.toLowerCase().includes(query.toLowerCase())).sort(byShipmentName) : shipments), [query, shipments]);
  useEffect(() => document.documentElement.classList.add('-navigated'), [pathname]);
  useEffect(() => !document.documentElement.classList.remove('-navigated') && query ? document.documentElement.classList.add('-searching') : document.documentElement.classList.remove('-searching'), [query]);
  useEffect(() => query && pathname !== '/' && document.documentElement.classList.contains('-navigated') ? window.scrollTo(0, document.querySelector('section').offsetTop - document.querySelector('header').offsetHeight) : window.scrollTo(0, 0), [query, pathname]);

  return (
    <div className="cargoplanner">
      <header role="group">
        <div className="title"><Link to="/"><em>{title}</em></Link></div>
        <div className="search">
          <input type="text" id="search" value={query} onChange={onSearchInput} placeholder="Search" />
        </div>
        <div className="controls">
          <button onClick={load}>Load</button>
          <button onClick={save}>Save</button>
        </div>
      </header>
      <div className='viewport'>
      {(stored && <main>
        <section role="group">
        <Switch>
          <Route path="/:tag">
            <Shipment shipments={shipments} onUpdate={onShipmentUpdate}/>
          </Route>
          <Route path="/">
            <h3>Please select a topic.</h3>
          </Route>
        </Switch>
        </section>
        <aside>
          <ul>
            {found.map(shipment => <li key={tagify(shipment.name)}><Link to={tagify(shipment.name)}>{shipment.name}</Link></li>)}
          </ul>
        </aside>
      </main>) || <output className="warning">Load plan above to manage shipments</output>}
      </div>
    </div>
  );
}

export default App;