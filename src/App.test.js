import React from 'react';
import { render, screen } from '@testing-library/react';

import { Router } from 'react-router-dom'
import { createMemoryHistory } from 'history'
import App from './App';

Object.defineProperty(window, 'scrollTo', { value: () => {}, writable: true });

test('renders cargo planner link', () => {
  const memory = createMemoryHistory();

  render(
    <Router history={memory}>
      <App />
    </Router>
  );

  const linkElement = screen.getByText(/Cargo Planner/i);
  expect(linkElement).toBeInTheDocument();
});
